package ru.itis;

public class Main {

    private static Boolean valueOf(boolean b) {
        return (b ? Boolean.TRUE : Boolean.FALSE);
    }

    public static void main(String[] args) {
	// write your code here
        System.out.println(valueOf(true));
        System.out.println(valueOf(false));
    }
}
