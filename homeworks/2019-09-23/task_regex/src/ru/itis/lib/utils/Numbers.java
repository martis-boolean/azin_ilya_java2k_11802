package ru.itis.lib.utils;

import java.util.Random;

public class Numbers {
    private static Random randomizer = new Random();

    public static boolean isPositive(int number) {
        return number > 0;
    }

    public static boolean isValid(Integer integer) {
        return integer != null;
    }

    public static int randomInt(int min, int max) {
        return randomInt(max - min) + min;
    }

    public static int randomInt(int max) {
        return randomizer.nextInt(max);
    }
}
