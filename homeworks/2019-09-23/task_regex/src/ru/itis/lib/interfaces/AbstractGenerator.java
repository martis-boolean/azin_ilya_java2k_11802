package ru.itis.lib.interfaces;

public abstract class AbstractGenerator<T> implements IGenerator<T> {
    protected int callsAmount = 0;

    protected void countCall() {
        this.callsAmount++;
    }

    public int getCallsAmount() {
        return this.callsAmount;
    }

    @Override
    public T[] generate(int amount) {
        T[] generated_items = (T[]) new Object[amount];

        for (int i = 0; i < amount; i++) {
            generated_items[i] = this.generate();
        }

        return generated_items;
    }
}
