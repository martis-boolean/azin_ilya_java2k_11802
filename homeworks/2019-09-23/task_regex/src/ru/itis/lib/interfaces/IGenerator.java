package ru.itis.lib.interfaces;

public interface IGenerator<T> {
    T generate();
    T[] generate(int amount);
}
