package ru.itis.lib.interfaces;

public interface IValidateAble<T> {
    boolean isValid(T item);
}
