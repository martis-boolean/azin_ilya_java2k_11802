package ru.itis.lib.interfaces;

public interface IChecker {
    boolean check();
}
