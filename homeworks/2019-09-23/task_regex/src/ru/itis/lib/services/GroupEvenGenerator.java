package ru.itis.lib.services;

import ru.itis.lib.interfaces.IValidateAble;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GroupEvenGenerator extends PositiveIntGenerator implements IValidateAble<Integer> {
    @Override
    public Integer generate() {
        Integer generated_int = null;
        while (!this.isValid(generated_int)) {
            generated_int = super.generate();
        }
        return generated_int;
    }

    @Override
    public boolean isValid(Integer item) {
        if (item == null) {
            return false;
        }

        int GROUP_AMOUNT = 2;
        String regex = "[02468][02468]";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(item.toString());

        int count = 0;
        while (matcher.find()) {
            if (++count == GROUP_AMOUNT) {
                return true;
            }
        }
        return false;
    }
}
