package ru.itis.lib.services;

import ru.itis.lib.interfaces.IValidateAble;
import ru.itis.lib.utils.Numbers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ThreeEvenGenerator extends PositiveIntGenerator implements IValidateAble<Integer> {

    @Override
    public Integer generate() {
        Integer generated_int = null;
        while (!this.isValid(generated_int)) {
            generated_int = super.generate();
        }
        return generated_int;
    }

    @Override
    public boolean isValid(Integer item) {
        if (!Numbers.isValid(item)) {
            return false;
        }

        String regex = "[02468][02468][02468]";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(item.toString());
        return !matcher.find();
    }
}
