package ru.itis.lib.services;

import ru.itis.lib.interfaces.AbstractGenerator;
import ru.itis.lib.interfaces.IValidateAble;
import ru.itis.lib.utils.Numbers;


// FIXME: counter call sys

public class PositiveIntGenerator extends AbstractGenerator<Integer> implements IValidateAble<Integer> {
    @Override
    public Integer generate() {
        // TODO: ItersLimit
        Integer generated_int = null;
        // Generate loop
        while (!this.isValid(generated_int)) {
            // FIXME: bound
            this.countCall();
            generated_int = Numbers.randomInt(10000, 99999);
        }

        return generated_int;
    }

    @Override
    public boolean isValid(Integer item) {
        return Numbers.isValid(item) && Numbers.isPositive(item);
    }
}
