package ru.itis;

import ru.itis.lib.interfaces.AbstractGenerator;
import ru.itis.lib.services.GroupEvenGenerator;
import ru.itis.lib.services.ThreeEvenGenerator;

// FIXME: Objects -> Integer
public class Main {
    public static void main(String[] args) {
        task1();
        task2();
    }

    private static void task1() {
        System.out.println(">  1. Вывести первые 10, в которых нет трех четных цифр подряд.");
        generateAndPrintCallsAmount(new ThreeEvenGenerator());
        System.out.println(".................................");
    }

    private static void task2() {
        System.out.println(">  2. Вывести первые 10, в которых есть хотя бы две группы из 2 четных цифр.");
        generateAndPrintCallsAmount(new GroupEvenGenerator());
        System.out.println(".................................");
    }


    private static void generateAndPrintCallsAmount(AbstractGenerator<Integer> generator) {
        int GENERATION_LIMIT = 10;
        Object[] generated = generator.generate(GENERATION_LIMIT);

        System.out.printf("Сгенерировано всего чисел: %s\n", generator.getCallsAmount());
        System.out.printf("Сгенерированные %s чисел:\n", GENERATION_LIMIT);
        for (int i = 0; i < GENERATION_LIMIT; i++) {
            System.out.printf("%s. %s\n", i + 1, generated[i]);
        }
    }
}
